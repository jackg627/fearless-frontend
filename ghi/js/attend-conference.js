// PULL CONFERENCE DATA FROM API
window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
// GET CONFERENCE DATA FOR DROPDOWN TAB
      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }
//SET LOADING SPINNER WHILE CONFERENCES ARE LOADING, HIDE SPINNER WHEN LOADED
      const spinnerTag = document.getElementById('loading-conference-spinner');
      spinnerTag.classList.add('d-none');

      selectTag.classList.remove('d-none');

// SEND NEW ATTENDEE FORM DATA TO DATABASE
      //Get attendee form element by its id
      const formTag = document.getElementById('create-attendee-form');
      //Add event handler for the 'submit' event
      formTag.addEventListener('submit', async event => {
        //Prevent the default browser event from happening
        event.preventDefault();

        //Create a 'FormData' object from the form
        const formData = new FormData(formTag);
        //Get a new object from the form data's entries
        const json = JSON.stringify(Object.fromEntries(formData));
        //Create variable to hold Attendees API URL
        const attendeesUrl = 'http://localhost:8001/api/attendees/';
        //Create options for the FETCH
        const fetchConfig = {
            method: 'post',
            body: json,
            headers: {
                'Content-Type': 'application/json'
            }
        }

        //Make the FETCH using the 'await' keyword to the Attendees API URL
        const response = await fetch(attendeesUrl, fetchConfig);

        if (response.ok) {
            //reset the form tag
            formTag.reset();
            //add the 'd-none' to the form tag
            formTag.classList.add('d-none')
            //set a variable to hold the html Success Message element
            const success = document.getElementById('success-message')
            //remove the 'd-none' from the success alert
            success.classList.remove('d-none')
            //set the new attendee as the json response
            const newAttendee = await response.json();
            //print the new attendee to the console
            console.log(newAttendee)

        }



      })


    }

  });
