window.addEventListener('DOMContentLoaded', async () => {
    //PULL LOCATION DATA FROM API
    const url = 'http://localhost:8000/api/locations/';

    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();

        const selectTag = document.getElementById('location');
        // GET LOCATION DATA FOR DROPDOWN TAB
        for (let location of data.locations) {
            const option = document.createElement('option')
            option.value = location.id;
            option.innerHTML = location.name;
            selectTag.appendChild(option);
        }

    }
    // SEND NEW CONFERENCE FORM DATA TO DATABASE
    const formTag = document.getElementById('create-conference-form');

    formTag.addEventListener('submit', async event => {
        event.preventDefault();

        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        //console.log(json)
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: 'post',
            body: json,
            headers: {
                'Content-Type': 'application/json'
            }
        }

        const response = await fetch(conferenceUrl, fetchConfig);

        if (response.ok) {
            formTag.reset();
            const newConference = await response.json();
            console.log(newConference)
        }


    });

});
