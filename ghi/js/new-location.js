
window.addEventListener('DOMContentLoaded', async () => {
    //PULL STATE DATA FROM API
    const url = 'http://localhost:8000/api/states/';

    const response = await fetch(url);
    if (response.ok) {

        const data = await response.json();

        const selectTag = document.getElementById('state');

        //GET STATE DATA FOR DROPDOWN TAB
        for (let state of data.states) {
            const option = document.createElement('option');
            //console.log(option)
            option.value = state.abbreviation; //
            //console.log(option.value) //
            option.innerHTML = state.name; //
            //console.log(option.innerHTML) //
            selectTag.appendChild(option);
            //console.log(option.value)
        }
    }

    // SEND NEW LOCATION FORM DATA TO DATABASE
    const formTag = document.getElementById('create-location-form');

    formTag.addEventListener('submit', async event => {
        event.preventDefault();

        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));

        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: 'post',
            body: json,
            headers: {
                'Content-Type': 'application/json'
            }
        }

        const response = await fetch(locationUrl, fetchConfig);

        if (response.ok) {
            formTag.reset();
            const newLocation = await response.json();

        }

    });


});
