function createCard(name, description, pictureUrl, startDate, startMonth, startYear, endDate, endMonth, endYear, location) {
    return `
      <div class="card shadow p-3 mb-5 bg-body rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <p class = "card-loc text-muted">${location}</p>
          <p class="card-text">${description}</p>
          <h6 class="card-subtitle mb-2 text-muted">${startMonth}/${startDate}/${startYear} - ${endMonth}/${endDate}/${endYear}</h6>
        </div>
      </div>
    `;
  }


window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try {
    const response = await fetch(url);
    if (!response.ok) {
        //do something if response is bad
    } else {
        const data = await response.json();

        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
              const details = await detailResponse.json();

              const name = details.conference.name;
              const description = details.conference.description;
              const pictureUrl = details.conference.location.picture_url;
              const location = details.conference.location.name;

              const isoStart = details.conference.starts;
              const start = new Date(isoStart);
              const startMonth = start.getMonth() + 1;

              const isoEnd = details.conference.ends;
              const end = new Date(isoEnd);
              let protoEndMonth = end.getMonth()
              let endMonth = protoEndMonth + 1;

              let startDate = start.getDate();
              let startYear = start.getFullYear();


              let endDate = end.getDate();
              let endYear = end.getFullYear();

              const html = createCard(name,
                                      description,
                                      pictureUrl,
                                      startDate,
                                      startMonth,
                                      startYear,
                                      endDate,
                                      endMonth,
                                      endYear,
                                      location);
              const column = document.querySelector('.col');
              column.innerHTML += html;
            }

        }
      }
    } catch (e) {
        //figure out what to do if error is raised
    }
});
