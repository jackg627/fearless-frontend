//ADD EVENT LISTENER FOR DOM LOADED
window.addEventListener('DOMContentLoaded', () => {
      //make a variable to hold 'login-form'
      const form = document.getElementById('login-form');
      //add event handler for form submission
      form.addEventListener('submit', async event => {
        //Prevent the default browser event from happening
        event.preventDefault();

        //Create a 'FormData' object from the form
        // and get a new Object from the form data's entries
        //const data = Object.fromEntries(new FormData(form))
        //Create options for the FETCH
        const fetchOptions = {
            method: 'post',
            body: new FormData(form),
            credentials: 'include',
            //headers: {                      // Commented out bc DJWTO doesn't take JSON
            //    'Content-Type': 'application/json',
            //}
        };
        //Create a variable to hold the Login url
        const loginUrl = 'http://localhost:8000/login/';
        //Make the FETCH using the 'await' keyword to the Login URL
        const response = await fetch(loginUrl, fetchOptions);
        //If the response is OK:
        if (response.ok) {
            //????
            window.location.href = '/';
        }   else {
            console.error(response);
        }
      });
});
